
# Références législatives

* [Code général des collectivités territoriales](https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000006070633)
* [Toutes notes dotations de la DGCL](http://www.dotations-dgcl.interieur.gouv.fr/consultation/informations_repartition.php)

## Dotation globale de fonctionnement (DGF)

La principale dotation de fonctionnement de l'État aux collectivités territoriales.
Au total, elle comporte 12 dotations (4 pour les communes, 2 pour les EPCI, 4 pour les départements et 2 pour les régions) qui se déclinent elles-mêmes en plusieurs parts ou fractions. Source (consultée en 03.2024) : [collectivites-locales.gouv.fr](https://www.collectivites-locales.gouv.fr/finances-locales/presentation-de-la-dotation-globale-de-fonctionnement-dgf)

* Guide pratique officiel, [La dotation globale de fonctionnement (DGF)](https://www.collectivites-locales.gouv.fr/files/Finances%20locales/guide_dgf2023%20gouv.pdf) (Mars 2023)

### Communes : Dotation forfaitaire (DF)

* Note DGCL, [DF 2024](http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=260) [[version à texte sélectionnable](https://cloud.leximpact.dev/index.php/s/tnzbY4wMpAcPjRb)]
* Note DGCL, [DF 2023](http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=220)
* Note DGCL, [DF 2022](http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=188)
* Note DGCL, [DF 2021](http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=142)
* Note DGCL, [DF 2020](http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=115)
* Note DGCL, [DF 2019](http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=92)

* [collectivites-locales.gouv.fr, DF 2019](https://www.collectivites-locales.gouv.fr/files/files/dgcl_v2/FLAE_circulaires_10_fevrier2016/note_dinformation_2019_dfcom_-_vdef2.pdf)

> Documents complémentaires pouvant servir à la compréhension des mécanismes de calcul :
> * [Rapport du Gouvernement au Parlement, Historique et analyse des effets de l’introduction d’un coefficient logarithmique](https://www.banquedesterritoires.fr/sites/default/files/2019-12/Coefficient%20logarithmique%20-%20Rapport%20global%20%282%29.pdf)
> * [collectivites-locales.gouv.fr, DF 2015](https://www.collectivites-locales.gouv.fr/files/files/noteinfo_dotationforfaitairecommunes.pdf)
> * [amf.asso.fr, DF 2016](http://medias.amf.asso.fr/docs/DOCUMENTS/AMF_14463_NOTE.pdf)
> * [yvelines.gouv.fr, DF 2018](http://www.yvelines.gouv.fr/content/download/15362/97278/file/Annexe%20de%20calcul-2018%20Dotation%20forfaitaire.pdf)

### Communes : Dotation d'aménagement

#### Communes : Dotation de Solidarité Rurale (DSR)

* Note DGCL, [DSR 2024](http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=258) [[version à texte sélectionnable](https://cloud.leximpact.dev/index.php/s/aG9NwmjWGqYeBgH)]
* Note DGCL, [DSR 2023](http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=223)
* Note DGCL, [DSR 2022](http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=181)
* Note DGCL, [DSR 2021](http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=145)
* Note DGCL, [DSR 2020](http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=123)
* Note DGCL, [DSR 2019](http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=94)

#### Communes : Dotation de Solidarité Urbaine et de cohésion sociale (DSU)

* Note DGCL, [DSU 2024](http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=257) [[version à texte sélectionnable](https://cloud.leximpact.dev/index.php/s/SAzCkDXtDKNAnqz)]
* Note DGCL, [DSU 2023](http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=222)
* Note DGCL, [DSU 2022](http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=179)
* Note DGCL, [DSU 2021](http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=140)
* Note DGCL, [DSU 2020](http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=120)
* Note DGCL, [DSU 2019](http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=93)

## Communes : Dotation en faveur des communes nouvelles

Instituée par l'[article 134 de la Loi n°2023-1322 du 29 décembre 2023 de finances pour 2024](https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000048727488).

* Note DGCL, [Dotation en faveur des communes nouvelles 2024](http://www.dotations-dgcl.interieur.gouv.fr/consultation/documentAffichage.php?id=262) [[version à texte sélectionnable](https://cloud.leximpact.dev/index.php/s/95ReWjj2xX5wtWA)]

## Communes : Sources des données

* [data/2019-communes-criteres-repartition.csv](https://www.data.gouv.fr/fr/datasets/criteres-de-repartition-des-dotations-versees-par-letat-aux-collectivites-territoriales/)
