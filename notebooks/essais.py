from openfisca_core.simulation_builder import SimulationBuilder
from openfisca_france_dotations_locales import CountryTaxBenefitSystem as DotationsLocales
import numpy as np

sb_dot = SimulationBuilder()
dotations = DotationsLocales()
dot = sb_dot.build_default_simulation(dotations, count=3)
dot.set_input("dsr_fraction_cible", 2020, np.array([3_000, 2, 1]))
dot.set_input("dsr_fraction_bourg_centre", 2020, np.array([1, 3_000, 10_000]))
dot.set_input("dsr_fraction_perequation", 2020, np.array([1, 3_000, 10_000]))
dot.trace = True
dsr = dot.calculate('dotation_solidarite_rurale', 2020)
dot.tracer.print_computation_log()
