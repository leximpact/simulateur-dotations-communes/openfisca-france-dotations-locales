# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = "OpenFisca-France-Dotations-Locales",
    version = "4.1.1",
    author = "LexImpact Team",
    author_email = "leximpact@an.fr",
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "License :: OSI Approved :: GNU Affero General Public License v3",
        "Operating System :: POSIX",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.11",
        "Topic :: Scientific/Engineering :: Information Analysis",
        ],
    description = "[EN] OpenFisca tax and benefit system for France State endowments to local authorities. \
        [FR] Modèle de microsimulation OpenFisca dédié aux dotations de l'État aux collectivités territoriales.",
    keywords = "France microsimulation local tax endowment dotations territoires",
    license ="http://www.fsf.org/licensing/licenses/agpl-3.0.html",
    url = "https://git.leximpact.dev/leximpact/openfisca-france-dotations-locales",
    include_package_data = True,  # Will read MANIFEST.in
    data_files = [
        ("share/openfisca/openfisca-france-dotations-locales",
        ["CHANGELOG.md", "LICENSE", "README.md", "REFERENCES.md"]),
        ],
    install_requires = [
        "OpenFisca-Core[web-api] >=27.0,<42.0",
        ],
    extras_require = {
        "dev": [
            "pandas >= 2.2.1, < 3.0.0",
            "autopep8 >= 2.1.0, < 3.0.0",
            "flake8 >=6.0.0, < 7.0.0",
            "pycodestyle >=2.10.0, < 3.0",
            "jupyter >= 1.0.0, < 2.0.0"
            ]
        },
    packages=find_packages(),
    )
